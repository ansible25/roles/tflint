Tflint
=========

Role that installs Tflint

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Tflint on targeted machine:

    - hosts: servers
      roles:
         - tflint

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
